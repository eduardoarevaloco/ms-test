#!/bin/bash

set -e

## VAULT LOGIN =====================================================

    DOCKER_LOG_LEVEL=${DOCKER_LOG_LEVEL:-DEBUG}
    eval LOG_LEVEL_${DOCKER_LOG_LEVEL}
    B_LOG --stdout true # enable logging over stdout

    export PORT0=${PORT0:-"8080"}

    declare -a VAULT_HOSTS
    IFS_OLD=$IFS
    IFS=',' read -r -a VAULT_HOSTS <<< "$VAULT_HOST"

    declare -a MARATHON_ARRAY
    OLD_IFS=$IFS
    IFS='/' read -r -a MARATHON_ARRAY <<< "$MARATHON_APP_ID"
    IFS=$OLD_IFS

    INFO "Trying to login in Vault"
    # Approle login from role_id, secret_id
    if [ "xxx$VAULT_TOKEN" == "xxx" ];
    then
       INFO "Login in vault..."
       login
       if [[ ${code} -ne 0 ]];
       then
           ERROR "  - Something went wrong log in in vault. Exiting..."
           return ${code}
       fi
    fi
    INFO "  - Logged!"

## SERVICE NAME ====================================================

    MARATHON_SERVICE_NAME=${MARATHON_ARRAY[-1]}
    MARATHON_SERVICE_NAME=$(echo $MARATHON_SERVICE_NAME | sed -E 's/(.*)-\d*$/\1/')

## TRUSTSTORE  ====================================================

    INFO "Getting ca-bundle"
    getCAbundle "/data/resources" "PEM" \
    && INFO "OK: Getting ca-bundle" \
    || INFO "Error: Getting ca-bundle"

    ${JAVA_HOME}/bin/keytool -noprompt -import -storepass changeit -file /data/resources/ca-bundle.pem -alias ${MARATHON_SERVICE_NAME} -keystore ${JAVA_HOME}/lib/security/cacerts

## RUN JAVA APP ===================================================

    JAVA_ARGS="--server.port=${PORT0}"

    HEAP_PERCENTAGE=${HEAP_PERCENTAGE:-"80"}
    JAVA_TOOL_OPTIONS=${JAVA_TOOL_OPTIONS:-"-XX:+UseG1GC -XX:MaxRAMPercentage=${HEAP_PERCENTAGE} -XshowSettings:vm"}

    JAVA_CMD="java ${JAVA_TOOL_OPTIONS} -jar /data/app.jar ${JAVA_ARGS}"

    INFO "Starting Spring Boot Service !"

    ${JAVA_CMD}
