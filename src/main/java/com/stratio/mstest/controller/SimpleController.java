package com.stratio.mstest.controller;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class SimpleController {

  @GetMapping("/")
  ResponseEntity<Map> echoGet(@RequestParam Map requestParams) {
    return ResponseEntity.ok()
        .body(requestParams);
  }

  @PostMapping("/")
  public ResponseEntity<Map> echoPost(@RequestBody Map requestBody) {
    return ResponseEntity.created(UriComponentsBuilder.fromPath("/").build().toUri()).body(requestBody);
  }

  @PutMapping("/")
  public ResponseEntity<Map> echoPut(@RequestBody Map requestBody) {
    return ResponseEntity.accepted().body(requestBody);
  }

}
