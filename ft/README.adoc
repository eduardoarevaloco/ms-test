# Integration Test for service mstest
For the correct execution of the tests it is necessary to lift the service with the profile "ft".

```
mvn spring-boot:run -Dspring.profiles.active=ft
```
When you do this, you load the test data into the database specified in _application-ft.yml_

Execution of the integration test project:
```
mvn clean verify --file it/pom.xml -DHost=http://localhost -DPort=5001
```
If the parameters are not specified, they are loaded by default Host=http://localhost y Port=5001

In report result of the execution the file can be seen in a vegan browser {porject_dir}/ft/target/site/serenity/index.html