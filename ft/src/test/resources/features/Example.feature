//TODO: @Luis include functional description of the class.

@EndToEnd
Feature: Example Feature to explain how use it

  Scenario Outline: Feature example of use
    When needed explain the <FeatureId> for the example <ExampleId>
    Then user can read the response with status code: <cod_respuesta>
    And new json report is create with name <json_file>

    Examples:
      |FeatureId    |ExampleId  |cod_respuesta  |json_file                      |
      |"426"        |"273617"   |'200'          |"FijarconsultaResponse.json"   |
