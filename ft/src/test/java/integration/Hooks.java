package com.stratio.mstest;

import cucumber.api.java.Before;
import io.restassured.RestAssured;
import lombok.extern.slf4j.Slf4j;

//TODO: @Luis include functional description of the class.
@Slf4j
public class Hooks {

  private static boolean dunit = false;

  @Before
  public void beforeAll() throws Exception {
    if (!dunit) {
      if (System.getProperty("Host") == null) {
        String host = "http://localhost";
        RestAssured.baseURI = host;
      } else {
        // you can specify it using -DHost=http://localhost
        RestAssured.baseURI = System.getProperty("Host");
      }

      if (System.getProperty("Port") == null) {
        int port = 9090;
        RestAssured.port = port;
      } else {
        // you can specify it using -DPort=8080
        RestAssured.port = Integer.parseInt(System.getProperty("Port"));
      }
      log.debug("Target service host: " + RestAssured.baseURI);
      log.debug("Target service port: " + RestAssured.port);
    }

    dunit = true;
  }

}
