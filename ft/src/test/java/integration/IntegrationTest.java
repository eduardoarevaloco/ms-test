package com.stratio.mstest;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import org.junit.runner.RunWith;

//TODO: @Luis include functional description of the class.
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
    features = {"src/test/resources/features"}, tags = {"not @NotImplemented", "not @EndToEnd"}
)
public class IntegrationTest {

}
